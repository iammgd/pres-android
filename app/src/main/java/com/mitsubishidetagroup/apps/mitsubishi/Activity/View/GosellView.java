package com.mitsubishidetagroup.apps.mitsubishi.Activity.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosellTimDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosurveyDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GoprosesFormScreen;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GosellAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GosurveyAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Gosell;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Gosurveys;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GosellView extends AppCompatActivity {
    List<Gosell> gosellData = new ArrayList<Gosell>();
    ListView list;
    private SQLiteHandler db;
    private static String URL_GOSELL;
    GosellAdapter adapter;
    private StringRequest strReq;
    private String user_id, user_type;
    ProgressDialog pDialog;
    JSONArray str_json = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gosell_view);

        inisialisasi();
        callGoSell();


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String id_gosell = ((TextView)view.findViewById(R.id.hiddenID_gosell)).getText().toString();

                //if login by gosell tim
                Intent i = new Intent(GosellView.this, GosellTimDetail.class);
                i.putExtra("id_gosell", id_gosell);
                startActivity(i);
            }
        });
    }

    public void inisialisasi(){
        list = (ListView) findViewById(R.id.list_gosell);
        db = new SQLiteHandler(getApplicationContext());
        user_id = db.getUserDetails().get("uid");
        user_type = db.getUserDetails().get("type_id");
        URL_GOSELL = AppConfig.URL_GETGOSELL_TIM + "?user_id="+user_id+"&user_type="+user_type;
        adapter = new GosellAdapter(GosellView.this, gosellData);
        //adapter.setCustomButtonListner(getActivity(),this);
        list.setAdapter(adapter);
    }
    public void callGoSell() {
        showProgressDialog();

        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(URL_GOSELL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("CEK RESPONSES: ", response.toString());
                hideProgressDialog();
                if (response.length() > 0) {
                    // Parsing json
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            Gosell gosell = createGosellFromJSONObject(obj);
                            gosellData.add(gosell);

                        } catch (JSONException e) {
                            Log.e("CEK ERROR REPONSES: ", "JSON Parsing error: " + e.getMessage());
                            hideProgressDialog();
                        }
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("CEK ERROR: "+error.getMessage(), "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
    }

    public void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(GosellView.this);
            pDialog.setMessage("Get data, please wait...");
            pDialog.setCancelable(false);
        }

        pDialog.show();
    }

    public void hideProgressDialog() {

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    private Gosell createGosellFromJSONObject(JSONObject object) throws JSONException {

        Gosell gosell = new Gosell();
        gosell.setId(object.getInt("id"));
        gosell.setCustomer_name(object.getString("customer_name"));
        gosell.setProfit(object.getString("profit"));
        gosell.setVehicle_name(object.getString("vehicle_name"));
        gosell.setStatus_validasi(object.getString("status_validasi"));
        return gosell;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(GosellView.this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.legenda,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent i;
        switch (item.getItemId()){
            case R.id.legenda:
                Toast.makeText(getApplicationContext(), "Legenda terbuka.", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }
}
