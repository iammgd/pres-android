package com.mitsubishidetagroup.apps.mitsubishi.Config;

/**
 * Created by zahid on 9/2/2017.
 */

public class AppConfig {
    public static String URL_IP = "http://192.168.43.154";
    public static String URL_GAMBAR_KTP = URL_IP + "/proj-mitsuweb/public/";

    public static String URL_CMO = URL_IP + "/proj-mitsuweb/public/api/gosurvey/getcmo";
    public static String URL_LEASING = URL_IP + "/proj-mitsuweb/public/api/gosurvey/getleasing";
    // Login
    public static String URL_LOGIN = URL_IP + "/proj-mitsuweb/public/api/login";
    public static String URL_REGISTER = URL_IP + "/proj-mitsuweb/public/api/users";

    // Gopros
    public static String URL_CREATEGOPROSES = URL_IP + "/proj-mitsuweb/public/api/goproses/create";
    public static String URL_GETGOPROSES = URL_IP + "/proj-mitsuweb/public/api/goproses/index";
    public static String URL_GOPROSESDETAIL = URL_IP + "/proj-mitsuweb/public/api/goproses/detail/";
    public static String URL_VEHICLE = URL_IP + "/proj-mitsuweb/public/api/goproses/vehicle/index";

    // Gosurvey
    public static String URL_CREATEGOSURVEY = URL_IP + "/proj-mitsuweb/public/api/gosurvey/create/"; //harus pake ID dari goprosnya
    public static String URL_GETGOSURVEY = URL_IP + "/proj-mitsuweb/public/api/gosurvey/index";
    public static String URL_GOSURVEYDETAIL = URL_IP + "/proj-mitsuweb/public/api/gosurvey/detail/";
    public static String URL_GOSURVEYACC = URL_IP + "/proj-mitsuweb/public/api/gosurvey/accept/";

    // Goorder
    public static String URL_GOORDERPENOLAKAN = URL_IP + "/proj-mitsuweb/public/api/goorder/detail/penolakan/";

    //jendral
    public static String URL_GETGOORDER_JENDRAL = URL_IP + "/proj-mitsuweb/public/api/goorder/jendral/index";
    public static String URL_GOORDERDETAIL_JENDRAL = URL_IP + "/proj-mitsuweb/public/api/goorder/jendral/detail/";
    public static String URL_GOORDERREQUEST_JENDRAL = URL_IP + "/proj-mitsuweb/public/api/goorder/jendral/request/"; // /+id_goorder;

    //panglima
    public static String URL_GETGOORDER_PANGLIMA = URL_IP + "/proj-mitsuweb/public/api/goorder/panglima/index";
    public static String URL_GOORDERDETAIL_PANGLIMA = URL_IP + "/proj-mitsuweb/public/api/goorder/panglima/detail/";
    public static String URL_GOORDERREJECT_PANGLIMA = URL_IP + "/proj-mitsuweb/public/api/goorder/panglima/reject/"; // /+id_goorder;
    public static String URL_GOORDERACC_PANGLIMA = URL_IP + "/proj-mitsuweb/public/api/goorder/panglima/accept/"; // /+id_goorder;


    //GOSELL

    //team
    public static String URL_GETGOSELL_TIM = URL_IP + "/proj-mitsuweb/public/api/gosell/gosellteam/index"; // ?id_user=2
    public static String URL_DETAILGOSELL_TIM = URL_IP + "/proj-mitsuweb/public/api/gosell/gosellteam/detail/"; // 1 atau 2 atau 3
    public static String URL_FILLGOSELL = URL_IP + "/proj-mitsuweb/public/api/gosell/gosellteam/fill/"; // 1 atau 2 atau 3 dan paramsnya : gosell_team_id by login dan no_rangka


}