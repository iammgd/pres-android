package com.mitsubishidetagroup.apps.mitsubishi.Data;

public class Sales {
    private int id,phone_number,supervisor_id;
    private String name,email,created_at,updated_at;

    Sales(int id,int phone_number,int supervisor_id,String name,String email,String created_at,String updated_at){
        this.id=id;
        this.phone_number=phone_number;
        this.supervisor_id=supervisor_id;
        this.name=name;
        this.email=email;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

    public void setSupervisor_id(int supervisor_id) {
        this.supervisor_id = supervisor_id;
    }
}
