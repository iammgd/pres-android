package com.mitsubishidetagroup.apps.mitsubishi.Data;

public class Customers {
    private int id,no_ktp,phone_number;
    private String name,photo_ktp,created_at,updated_at;

    Customers(int id,int no_ktp,int phone_number,String name,String photo_ktp,String created_at,String updated_at){
        this.id=id;
        this.no_ktp=no_ktp;
        this.phone_number=phone_number;
        this.name=name;
        this.photo_ktp=photo_ktp;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

    public void setNo_ktp(int no_ktp) {
        this.no_ktp = no_ktp;
    }

    public void setPhoto_ktp(String photo_ktp) {
        this.photo_ktp = photo_ktp;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public int getNo_ktp() {
        return no_ktp;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public String getPhoto_ktp() {
        return photo_ktp;
    }
}
