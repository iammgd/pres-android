package com.mitsubishidetagroup.apps.mitsubishi.Data;

public class Cmos {
    private int id,phone_number;
    private String name,created_at,updated_at;
    Cmos(int id,int phone_number,String name,String created_at,String updated_at){
        this.id=id;
        this.phone_number=phone_number;
        this.name=name;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
