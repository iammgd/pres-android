package com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GoorderJendralPengajuanForm;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GosurveyFormScreen;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoorderJendralView;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GoorderJendralAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GoorderPenolakanAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Goorder;
import com.mitsubishidetagroup.apps.mitsubishi.Data.GoorderPenolakan;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GAMBAR_KTP;
import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GOORDERPENOLAKAN;

public class GoorderJendralDetail extends AppCompatActivity {

    private SQLiteHandler db;
    public String id_goorder, status_validasi;
    public int user_type;
    private final String url_detail  = AppConfig.URL_GOORDERDETAIL_JENDRAL;
    private final String url_detail_penolakan  = AppConfig.URL_GOORDERPENOLAKAN;
    TextView txtnama_customer, txtnotelp_customer, txtnoktp_customer, txtstatus, txtotr, txtbbn, txtdiskon, txttanggal_pengajuan, txtvehicle_name, txtsales_name, txtspv_name, txtkacab_name, txtprofit, txtongkir;

    List<GoorderPenolakan> goorderPenolakanData = new ArrayList<GoorderPenolakan>();
    ListView list;
    GoorderPenolakanAdapter adapter;
    private StringRequest strReq;
    Button button;

    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goorder_jendral_detail);
        inisialisasi();

        callDetailGoorder();
        callPenolakan();
    }

    private void inisialisasi()
    {
        txtnama_customer = (TextView) findViewById(R.id.nama_customer);
        txtnotelp_customer = (TextView) findViewById(R.id.notelp_customer);
        txtnoktp_customer = (TextView) findViewById(R.id.noktp_customer);
        txtstatus = (TextView) findViewById(R.id.status);
        txtotr = (TextView) findViewById(R.id.txtotr);
        txtbbn = (TextView) findViewById(R.id.txtbbn);
        txtdiskon = (TextView) findViewById(R.id.txtdiskon);
        txttanggal_pengajuan = (TextView) findViewById(R.id.tanggal_pengajuan);
        txtvehicle_name = (TextView) findViewById(R.id.vehicle_name);
        txtsales_name = (TextView) findViewById(R.id.sales_name);
        txtspv_name = (TextView) findViewById(R.id.spv_name);
        txtkacab_name = (TextView) findViewById(R.id.kacab_name);
        txtprofit = (TextView) findViewById(R.id.txtprofit);
        id_goorder = getIntent().getStringExtra("id_goorder");
        txtongkir = (TextView) findViewById(R.id.txtongkir);
        db = new SQLiteHandler(getApplicationContext());
        user_type= Integer.parseInt(db.getUserDetails().get("type_id"));

        list = (ListView) findViewById(R.id.list_goorder_penolakan);

        adapter = new GoorderPenolakanAdapter(GoorderJendralDetail.this, goorderPenolakanData);
        //adapter.setCustomButtonListner(getActivity(),this);
        list.setAdapter(adapter);
        button = findViewById(R.id.button);
        button.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View v )
            {
                if(user_type==6 || user_type == 7){
                    Toast.makeText(GoorderJendralDetail.this,
                            "Anda tidak memiliki akses untuk mengajukan GoOrder ini.", Toast.LENGTH_LONG).show();
                } else {
                    if(status_validasi.equals("1")){
                        Toast.makeText(GoorderJendralDetail.this,
                                "Gagal mengajukan kembali. GoOrder ini sudah masuk ke tahap GoSell.", Toast.LENGTH_LONG).show();
                    } else {
                        Intent i = new Intent(GoorderJendralDetail.this, GoorderJendralPengajuanForm.class);
                        i.putExtra("id_goorder", id_goorder);
                        startActivity(i);
                    }
                }

            }
        });
    }

    private void callDetailGoorder(){
        StringRequest strReq = new StringRequest(Request.Method.GET, url_detail + id_goorder, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("CEK RESPONSE: ", "Response " + response.toString());
                try {
                    JSONObject object  = new JSONObject(response);
                    SetTextFromOBJtoView(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON ERROR: ", "Detail Goproses Error: " + error.getMessage());
                Toast.makeText(GoorderJendralDetail.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "JSON OBJECT");
    }

    public void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(GoorderJendralDetail.this);
            pDialog.setMessage("Get data, please wait...");
            pDialog.setCancelable(false);
        }

        pDialog.show();
    }

    public void hideProgressDialog() {

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    public void callPenolakan() {
        showProgressDialog();
        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(url_detail_penolakan+id_goorder, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("CEK RESPONSES: ", response.toString());
                hideProgressDialog();
                if (response.length() > 0) {
                    // Parsing json
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            GoorderPenolakan goorderPenolakan = createGoorderPenolakanFromJSONObject(obj);
                            goorderPenolakanData.add(goorderPenolakan);

                        } catch (JSONException e) {
                            Log.e("CEK ERROR REPONSES: ", "JSON Parsing error: " + e.getMessage());
                            hideProgressDialog();
                        }
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("CEK ERROR: "+error.getMessage(), "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
    }

    private GoorderPenolakan createGoorderPenolakanFromJSONObject(JSONObject object) throws JSONException {

        GoorderPenolakan gp = new GoorderPenolakan();
        gp.setAlasan(object.getString("alasan_penolakan"));
        gp.setTanggal_penolakan(object.getString("tanggal_penolakan"));

        return gp;
    }


    private void SetTextFromOBJtoView(JSONObject object) throws JSONException {
        txtnama_customer.setText(object.getString("customer_name"));
        txtnotelp_customer.setText(object.getString("customer_phone"));
        txtnoktp_customer.setText(object.getString("customer_noktp"));
        txtstatus.setText(object.getString("status"));
        txtotr.setText(object.getString("otr"));
        txtbbn.setText(object.getString("bbn"));
        txtdiskon.setText(object.getString("diskon"));
        txttanggal_pengajuan.setText(object.getString("tanggal_pengajuan"));
        txtvehicle_name.setText(object.getString("vehicle_name"));
        txtsales_name.setText(object.getString("sales_name"));
        txtspv_name.setText(object.getString("spv_name"));
        txtkacab_name.setText(object.getString("kacab_name"));
        txtprofit.setText(object.getString("profit"));
        txtongkir.setText(object.getString("ongkir"));
        status_validasi = object.getString("status_validasi");

    }

    @Override
    public void onBackPressed() {
        finish();
        Intent i = new Intent(GoorderJendralDetail.this, GoorderJendralView.class);
        startActivity(i);
    }

}
