package com.mitsubishidetagroup.apps.mitsubishi.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mitsubishidetagroup.apps.mitsubishi.Data.Goorder;
import com.mitsubishidetagroup.apps.mitsubishi.Data.GoorderPenolakan;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import java.util.List;

/**
 * Created by megan on 27/05/2018.
 */

public class GoorderPenolakanAdapter extends BaseAdapter{
    private Activity activity;
    private List<GoorderPenolakan> goorderPenolakanData;
    private static LayoutInflater inflater=null;

    public GoorderPenolakanAdapter(Activity a, List<GoorderPenolakan> goorderPenolakanData) {
        activity = a;
        this.goorderPenolakanData=goorderPenolakanData;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return goorderPenolakanData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)

            vi = inflater.inflate(R.layout.activity_goorder_penolakan_list, null);
        ImageView imageView= (ImageView)vi.findViewById(R.id.imageView);

        TextView alasanPenolakan = (TextView)vi.findViewById(R.id.alasan_penolakan);
        TextView tanggalPenolakan = (TextView)vi.findViewById(R.id.tanggal_penolakan);

        GoorderPenolakan goorderPenolakan = goorderPenolakanData.get(position);
        alasanPenolakan.setText(goorderPenolakan.getAlasan());
        tanggalPenolakan.setText(goorderPenolakan.getTanggal_penolakan());
        return vi;
    }

}
