package com.mitsubishidetagroup.apps.mitsubishi.Data;

public class Leasings {
    private int id;
    private String name,created_at, updated_at;

    Leasings(int id,String name,String created_at,String updated_at){
        this.id=id;
         this.name=name;
         this.created_at=created_at;
         this.updated_at=updated_at;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
