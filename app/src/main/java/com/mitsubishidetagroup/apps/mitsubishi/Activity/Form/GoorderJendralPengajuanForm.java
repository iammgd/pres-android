package com.mitsubishidetagroup.apps.mitsubishi.Activity.Form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GoorderJendralDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosurveyDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Goorder;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import java.util.Hashtable;
import java.util.Map;

import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GOORDERREQUEST_JENDRAL;
import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GOSURVEYACC;

public class GoorderJendralPengajuanForm extends AppCompatActivity {
    private Button btnAjukan;
    private TextView txtotr, txtbbn, txtdiskon, txtongkir;
    private String URL_FORM = URL_GOORDERREQUEST_JENDRAL;
    private String id_goorder;
    private ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goorder_jendral_pengajuan_form);

        inisialisasi();

        btnAjukan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otr  = txtotr.getText().toString();
                String bbn = txtbbn.getText().toString();
                String diskon = txtdiskon.getText().toString();
                String ongkir = txtongkir.getText().toString();
                sendForm(otr,bbn,diskon, ongkir);
            }
        });
    }

    private void inisialisasi(){
        btnAjukan = (Button) findViewById(R.id.btnAjukan);
        txtotr = (TextView) findViewById(R.id.editOTR);
        txtbbn = (TextView) findViewById(R.id.editBBN);
        txtongkir = (TextView) findViewById(R.id.editOngkir);
        txtdiskon = (TextView) findViewById(R.id.editDiskon);
        id_goorder = getIntent().getStringExtra("id_goorder");
    }

    private void sendForm(final String otr, final String bbn, final String diskon, final String ongkir) {
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Sending...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_FORM + id_goorder,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();
                        Log.d("test", "onResponse: "+s);


                        //Showing toast message of the response
                        //Toast.makeText(GosurveyFormACC.this, s, Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "GoOrder ini berhasil diajukan!", Toast.LENGTH_LONG).show();

                        // Setting after done
                        Intent i = new Intent(GoorderJendralPengajuanForm.this, GoorderJendralDetail.class);
                        i.putExtra("id_goorder", id_goorder);
                        startActivity(i);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        //Showing toast
                        Toast.makeText(GoorderJendralPengajuanForm.this, "error", Toast.LENGTH_LONG).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("otr", otr);
                params.put("bbn", bbn);
                params.put("discount", diskon);
                params.put("ongkir", ongkir);
                //returning parameters
                return params;
            }
            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
