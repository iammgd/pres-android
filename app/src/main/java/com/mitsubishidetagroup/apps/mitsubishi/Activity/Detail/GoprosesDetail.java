package com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GosellRangkaForm;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GosurveyFormScreen;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoprosesView;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GosurveyView;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GAMBAR_KTP;

public class GoprosesDetail extends AppCompatActivity {

    private SQLiteHandler db;
    private int user_type, status_validasi;
    NetworkImageView foto_ktp;
    TextView txtnama_customer, txtnotelp_customer, txtnoktp_customer, txtnama_komandan, txtnama_sales, txtjenis_kendaraan, txttanggal_gopros, txtsumber_prospek, txtprogress, txtstatus, txtjenis;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public String id_gopros;


    private final String url_detail  = AppConfig.URL_GOPROSESDETAIL;
    ProgressDialog pDialog;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goproses_detail);
        inisialisasi();
        button.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View v )
            {
                if(user_type==1 || user_type == 6 || user_type == 7){
                    Toast.makeText(GoprosesDetail.this,
                            "Anda tidak memiliki akses untuk mengajukan leasing.", Toast.LENGTH_LONG).show();
                } else {
                    if(status_validasi == 0){
                        Intent i = new Intent(GoprosesDetail.this, GosurveyFormScreen.class);
                        i.putExtra("id_gopros", id_gopros);
                        startActivity(i);
                    } else {
                        Toast.makeText(GoprosesDetail.this,
                                "Leasing sudah diajukan", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });
        callDetailGoproses();

    }

    public void inisialisasi(){
        foto_ktp = (NetworkImageView) findViewById(R.id.foto_ktp);
        txtnama_customer = (TextView) findViewById(R.id.nama_customer);
        txtnotelp_customer = (TextView) findViewById(R.id.notelp_customer);
        txtnoktp_customer = (TextView) findViewById(R.id.noktp_customer);
        txtnama_komandan = (TextView) findViewById(R.id.nama_komandan);
        txtnama_sales = (TextView) findViewById(R.id.nama_sales);
        txtjenis_kendaraan = (TextView) findViewById(R.id.jenis_kendaraan);
        txttanggal_gopros = (TextView) findViewById(R.id.tanggal_gopros);
        txtsumber_prospek = (TextView) findViewById(R.id.sumber_prospek);
        txtprogress = (TextView) findViewById(R.id.progress);
        txtstatus = (TextView) findViewById(R.id.gopros_status);
        txtjenis = (TextView) findViewById(R.id.gopros_jenis);
        id_gopros = getIntent().getStringExtra("id_gopros");
        db = new SQLiteHandler(getApplicationContext());
        user_type= Integer.parseInt(db.getUserDetails().get("type_id"));
        button = findViewById(R.id.button);
    }

    public void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(GoprosesDetail.this);
            pDialog.setMessage("Get data, please wait...");
            pDialog.setCancelable(false);
        }

        pDialog.show();
    }

    public void hideProgressDialog() {

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    private void callDetailGoproses(){
        showProgressDialog();
        StringRequest strReq = new StringRequest(Request.Method.GET, url_detail + id_gopros, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("CEK RESPONSE: ", "Response " + response.toString());
                hideProgressDialog();
                try {
                    JSONObject object  = new JSONObject(response);
                    SetTextFromOBJtoView(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON ERROR: ", "Detail Goproses Error: " + error.getMessage());
                Toast.makeText(GoprosesDetail.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideProgressDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "JSON OBJECT");
    }

    private void SetTextFromOBJtoView(JSONObject object) throws JSONException {
        String gambar_ktp  = URL_GAMBAR_KTP+ object.getString("foto_ktp");

        txtnama_customer.setText(object.getString("customer_name"));
        txtnotelp_customer.setText(object.getString("customer_phone"));
        txtnoktp_customer.setText(object.getString("customer_ktp"));
        txtnama_komandan.setText(object.getString("supervisor_name"));
        txtnama_sales.setText(object.getString("sales_name"));
        txtjenis_kendaraan.setText(object.getString("vehicle_name"));
        txttanggal_gopros.setText(object.getString("created_at"));
        txtsumber_prospek.setText(object.getString("source"));
        txtprogress.setText(object.getString("progress"));
        txtstatus.setText(object.getString("status"));
        txtjenis.setText(object.getString("jenis_transaksi_cash"));
        foto_ktp.setImageUrl(gambar_ktp, imageLoader);
        status_validasi = object.getInt("status_validasi");

    }

    @Override
    public void onBackPressed() {
        finish();
        Intent i = new Intent(GoprosesDetail.this, GoprosesView.class);
        startActivity(i);
    }

}
