package com.mitsubishidetagroup.apps.mitsubishi.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Goproses;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by megan on 25/05/2018.
 */

public class GoprosesAdapter extends BaseAdapter {
    private Activity activity;
    private List<Goproses> goprosesData;
    private static LayoutInflater inflater=null;

    public GoprosesAdapter(Activity a, List<Goproses> goprosesData) {
        activity = a;
        this.goprosesData=goprosesData;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return goprosesData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)

            vi = inflater.inflate(R.layout.activity_goproses_list, null);

        TextView custName = (TextView)vi.findViewById(R.id.txtCustName);
        TextView tanggalGopros = (TextView)vi.findViewById(R.id.txtCreatedAt);
        TextView status = (TextView)vi.findViewById(R.id.txtStatus);
        TextView vehicleName = (TextView)vi.findViewById(R.id.txtVehicleName);
        TextView hiddenid = (TextView)vi.findViewById(R.id.hiddenID);
        ImageView imageView= (ImageView)vi.findViewById(R.id.imageView);
        Goproses goproses = goprosesData.get(position);

        custName.setText(goproses.getCustomer_name());
        tanggalGopros.setText(goproses.getCreated_at());
        vehicleName.setText(goproses.getVehicle_name());
        status.setText(goproses.getStatus());
        hiddenid.setText( String.valueOf(goproses.getId()));
        if(goproses.getStatus_validasi().equals("1")){
            imageView.setImageResource(R.drawable.goprossubmited);
        } else if(goproses.getStatus_validasi().equals("2")){
            imageView.setImageResource(R.drawable.goprosaccepted);
        } else if(goproses.getStatus_validasi().equals("3")){
            imageView.setImageResource(R.drawable.goprosexpired); //ditolak
        } else{
            imageView.setImageResource(R.drawable.goprosdefault);
        }

        return vi;
    }

}