package com.mitsubishidetagroup.apps.mitsubishi.Activity.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosurveyDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GosurveyAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Gosurveys;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class GosurveyView extends AppCompatActivity {
    List<Gosurveys> gosurveyData = new ArrayList<Gosurveys>();
    ListView list;
    private SQLiteHandler db;
    private static String URL_GOSURVEY;
    GosurveyAdapter adapter;
    private StringRequest strReq;
    private String user_type, user_id;

    ProgressDialog pDialog;
    JSONArray str_json = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gosurvey_view);

        inisialisasi();
        callGoSurvey();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String id_gosurvey = ((TextView)view.findViewById(R.id.hiddenID_gosurvey)).getText().toString();
                Intent i = new Intent(GosurveyView.this, GosurveyDetail.class);
                i.putExtra("id_gosurvey", id_gosurvey);
                startActivity(i);
            }
        });
    }

    public void inisialisasi(){
        list = (ListView) findViewById(R.id.list_gosurvey);
        db = new SQLiteHandler(getApplicationContext());
        user_type = db.getUserDetails().get("type_id").toString();
        user_id = db.getUserDetails().get("uid").toString();
        URL_GOSURVEY = AppConfig.URL_GETGOSURVEY + "?user_id="+user_id + "&user_type="+user_type;
        adapter = new GosurveyAdapter(GosurveyView.this, gosurveyData);
        list.setAdapter(adapter);
    }

    public void callGoSurvey() {
        showProgressDialog();

        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(URL_GOSURVEY, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("CEK RESPONSES: ", response.toString());
                hideProgressDialog();
                if (response.length() > 0) {
                    // Parsing json
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            Gosurveys gosurvey = createGoprosesFromJSONObject(obj);
                            gosurveyData.add(gosurvey);

                        } catch (JSONException e) {
                            Log.e("CEK ERROR REPONSES: ", "JSON Parsing error: " + e.getMessage());
                            hideProgressDialog();
                        }
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("CEK ERROR: "+error.getMessage(), "Error: " + error.getMessage());
                Log.d("CEK RESPONSES type: ", user_type);
                hideProgressDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
    }

    public void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(GosurveyView.this);
            pDialog.setMessage("Get data, please wait...");
            pDialog.setCancelable(false);
        }

        pDialog.show();
    }

    public void hideProgressDialog() {

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    private Gosurveys createGoprosesFromJSONObject(JSONObject object) throws JSONException {

        Gosurveys gosurvey = new Gosurveys();
        gosurvey.setId(object.getInt("id"));
        gosurvey.setLeasing(object.getString("leasing"));
        gosurvey.setNama_cmo(object.getString("nama_cmo"));
        gosurvey.setTgl_masuk_leasing(object.getString("tgl_masuk_leasing"));
        gosurvey.setTgl_acc(object.getString("tgl_acc"));
        gosurvey.setStatus_validasi(object.getString("status_validasi"));
        return gosurvey;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(GosurveyView.this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.legenda,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent i;
        switch (item.getItemId()){
            case R.id.legenda:
                Toast.makeText(getApplicationContext(), "Legenda terbuka.", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }
}
