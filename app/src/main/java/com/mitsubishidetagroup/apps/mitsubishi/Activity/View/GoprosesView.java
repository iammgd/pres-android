package com.mitsubishidetagroup.apps.mitsubishi.Activity.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GoprosesDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GoprosesFormScreen;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GoprosesAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.JSONParser;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Goproses;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GoprosesView extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener  {
    private List<Goproses> goprosesData = new ArrayList<Goproses>();
    private ListView list;
    private SQLiteHandler db;
    private static String URL_GOPROSES;
    private GoprosesAdapter adapter;
    private StringRequest strReq;
    private SwipeRefreshLayout swipe;
    private int offSet = 0,no=0;
    private String user_type, user_id;

    ProgressDialog pDialog;
    JSONArray str_json = null;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goproses_view);
        inisialisasi();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String id_gopros = ((TextView)view.findViewById(R.id.hiddenID)).getText().toString();
                Intent i = new Intent(GoprosesView.this, GoprosesDetail.class);
                i.putExtra("id_gopros", id_gopros);
                startActivity(i);
            }
        });

        swipe.setOnRefreshListener(this);
        swipe.post(new Runnable() {
        @Override
        public void run() {
            swipe.setRefreshing(true);
            goprosesData.clear();
            adapter.notifyDataSetChanged();
            offSet=0;
            callGoProses(0);
        }
        });
        list.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    swipe.setRefreshing(true);
                    runnable = new Runnable() {
                        public void run() {
                            callGoProses(offSet);

                        }
                    };
                    swipe.postDelayed(runnable, 3000);
                }
            }

        });
    }

    public void inisialisasi(){
        list = (ListView) findViewById(R.id.list_goproses);
        db = new SQLiteHandler(getApplicationContext());
        user_id = db.getUserDetails().get("uid");
        user_type = db.getUserDetails().get("type_id");
        goprosesData.clear();
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        URL_GOPROSES = AppConfig.URL_GETGOPROSES + "?user_id=" + user_id + "&user_type=" + user_type;
        adapter = new GoprosesAdapter(GoprosesView.this, goprosesData);
        //adapter.setCustomButtonListner(getActivity(),this);
        list.setItemsCanFocus(false);
        list.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        goprosesData.clear();
        adapter.notifyDataSetChanged();
        offSet=0;
        callGoProses(0);
    }

    public void callGoProses(int page) {
        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(URL_GOPROSES, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("CEK RESPONSES: ", response.toString());
                        swipe.setRefreshing(false);
                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject obj = response.getJSONObject(i);
                                    Goproses goproses = createGoprosesFromJSONObject(obj);
                                    goprosesData.add(goproses);
                                    if (no > offSet){
                                        offSet = no;
                                    }

                                } catch (JSONException e) {
                                    Log.e("CEK ERROR REPONSES: ", "JSON Parsing error: " + e.getMessage());
                                    swipe.setRefreshing(false);
                                }
                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("CEK ERROR: "+error.getMessage(), "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
    }




    private Goproses createGoprosesFromJSONObject(JSONObject object) throws JSONException {

        Goproses goproses = new Goproses();
        goproses.setId(object.getInt("id"));
        goproses.setCustomer_name(object.getString("customer_name"));
        goproses.setSupervisor_name(object.getString("supervisor_name"));
        goproses.setSales_name(object.getString("sales_name"));
        goproses.setCustomer_ktp(object.getString("customer_ktp"));
        goproses.setCustomer_phone(object.getInt("customer_phone"));
        goproses.setVehicle_name(object.getString("vehicle_name"));
        goproses.setCreated_at(object.getString("created_at"));
        goproses.setSource(object.getString("source"));
        goproses.setProgress(object.getString("progress"));
        goproses.setFoto_ktp(object.getString("foto_ktp"));
        goproses.setStatus(object.getString("status"));
        goproses.setStatus_validasi(object.getString("status_validasi"));
        return goproses;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.gopros,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent i;
        switch (item.getItemId()){
            case R.id.tambah:
                if(user_type.equals("6") || user_type.equals("7")){
                    Toast.makeText(getApplicationContext(), "Anda tidak memiliki akses untuk menambahkan gopros.", Toast.LENGTH_SHORT).show();
                } else {
                    i = new Intent(GoprosesView.this, GoprosesFormScreen.class);
                    startActivity(i);
                }
                break;
            case R.id.legenda:
                Toast.makeText(getApplicationContext(), "Legenda terbuka.", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(GoprosesView.this, MainActivity.class);
        startActivity(i);
    }
}
