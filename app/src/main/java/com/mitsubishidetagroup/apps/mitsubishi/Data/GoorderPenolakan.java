package com.mitsubishidetagroup.apps.mitsubishi.Data;

/**
 * Created by megan on 27/05/2018.
 */

public class GoorderPenolakan {
    private String alasan;
    private String tanggal_penolakan;
    private String status_validasi;

    public GoorderPenolakan()
    {

    }

    GoorderPenolakan(String alasan, String tanggal_penolakan, String status_validasi)
    {
        this.alasan = alasan;
        this.tanggal_penolakan = tanggal_penolakan;
        this.status_validasi= status_validasi;
    }

    public void setStatus_validasi(String status_validasi) {
        this.status_validasi = status_validasi;
    }

    public String getStatus_validasi() {
        return status_validasi;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }

    public String getTanggal_penolakan() {
        return tanggal_penolakan;
    }

    public void setTanggal_penolakan(String tanggal_penolakan) {
        this.tanggal_penolakan = tanggal_penolakan;
    }


}
