package com.mitsubishidetagroup.apps.mitsubishi;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

import com.google.firebase.messaging.FirebaseMessaging;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GoorderJendralDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GoprosesFormScreen;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Other.AksesScreen;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Other.LoginScreen;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoorderJendralView;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoorderPanglimaView;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoprosesView;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GosellView;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GosurveyView;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SessionManager;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Goproses;

public class MainActivity extends AppCompatActivity {
    private SQLiteHandler db;
    private static final String TAG = "MainActivity";
    private int user_type;
    private ImageButton gopross,gosurveys, goorder,Gosell;
    private SessionManager session;
    private ImageView textView13;

    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        setContentView(R.layout.activity_main);
        gopross = findViewById(R.id.Gopross);
        gosurveys = findViewById(R.id.Gosurveys);
        goorder = findViewById(R.id.goorder);
        Gosell = findViewById(R.id.Gosell);

        user_type= Integer.parseInt(db.getUserDetails().get("type_id"));
        textView13 = findViewById(R.id.textView13);
        textView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
        if(user_type==5 || user_type == 4){
            gopross.setImageResource(R.drawable.goprosdisabled);
        }
        if(user_type==1||user_type==5 ||user_type==4){
            gosurveys.setImageResource(R.drawable.gosurveydisabled);
        }
        if(user_type==1||user_type==2||user_type==5){
            goorder.setImageResource(R.drawable.goorderdisabled);
        }
        if(user_type==1||user_type==2||user_type==3){
            Gosell.setImageResource(R.drawable.goselldisabled);
        }

        goorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user_type==1||user_type==2||user_type==5){
                    intent = new Intent(MainActivity.this, AksesScreen.class);
                    //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                    startActivity(intent);
                    finish();
                }else if(user_type==4){
                    intent = new Intent(MainActivity.this, GoorderPanglimaView.class);
                    //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                    startActivity(intent);
                    finish();
                }else if(user_type==3 || user_type == 6 || user_type == 7){
                    intent = new Intent(MainActivity.this, GoorderJendralView.class);
                    //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
        gopross.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(user_type==5 || user_type==4){
                   intent = new Intent(MainActivity.this, AksesScreen.class);
                   //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                   startActivity(intent);
                   finish();
               }else{
                   //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                   intent = new Intent(MainActivity.this, GoprosesView.class);
                   startActivity(intent);
                   finish();
               }

           }
        });
        gosurveys.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if(user_type==1||user_type==5 ||user_type==4){
                    intent = new Intent(MainActivity.this, AksesScreen.class);
                    //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                    startActivity(intent);
                    finish();
                }else{
                    intent = new Intent(MainActivity.this, GosurveyView.class);
                    //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
        Gosell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user_type==1||user_type==2||user_type==3) {
                    intent = new Intent(MainActivity.this, AksesScreen.class);
                    //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                    startActivity(intent);
                    finish();
                }else{
                    intent = new Intent(MainActivity.this, GosellView.class);
                    //intent = new Intent(MainActivity.this, GoprosesFormScreen.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
            String token = FirebaseInstanceId.getInstance().getToken();
            //Finally we need to implement a method to store this unique id to our server
            sendIdToServer(token);
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        }

        private void sendIdToServer(final String token) {
            //Creating a progress dialog to show while it is storing the data on server

            //getting the email entered
            final String email = "zahid_black11@yahoo.com";

            //Creating a string request
            StringRequest req = new StringRequest(Request.Method.POST, AppConfig.URL_REGISTER+"/"+db.getUserDetails().get("uid")+"/token",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //dismissing the progress dialog
                            //if the server returned the string success
                            if (response.trim().equalsIgnoreCase("success")) {
                                //Displaying a success toast
                            } else {
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    //adding parameters to post request as we need to send firebase id and email
                    params.put("token", token);
                    return params;
                }
            };

            //Adding the request to the queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(req);
        }
    private void logoutUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Do you want to logout ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                session.setLogin(false);

                db.deleteUsers();

                // Launching the login activity
                Intent intent = new Intent(MainActivity.this, LoginScreen.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        }
    }