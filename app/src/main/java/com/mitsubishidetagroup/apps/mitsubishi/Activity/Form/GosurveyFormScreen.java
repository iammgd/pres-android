package com.mitsubishidetagroup.apps.mitsubishi.Activity.Form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GoprosesDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoprosesView;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class GosurveyFormScreen extends AppCompatActivity {
    private ProgressDialog progressDialog ;
    private SQLiteHandler db;
    private ImageView imageView;
    private String ServerPath;
    private List<String> list_id = new ArrayList<String>();
    private List<String> list_id2 = new ArrayList<String>();
    private AwesomeValidation awesomeValidation;
    private Button btnSimpan;
    private int item;
    private int item2;
    private EditText editKet;
    private Spinner spinCMO, spinLeasing;
    public String id_gopros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gosurvey_form_screen);
        btnSimpan=findViewById(R.id.btnSimpan);
        spinLeasing=findViewById(R.id.spinLeasing);
        spinCMO=findViewById(R.id.spinCMO);
        db = new SQLiteHandler(getApplicationContext());
        editKet=findViewById(R.id.editKet);
        id_gopros = getIntent().getStringExtra("id_gopros");
        ServerPath = AppConfig.URL_CREATEGOSURVEY+id_gopros;
        loadSpinnerData(AppConfig.URL_LEASING);
        loadSpinnerData2(AppConfig.URL_CMO);

        spinLeasing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                ((TextView) arg0.getChildAt(0)).setTextColor(Color.BLACK);
                item = spinLeasing.getSelectedItemPosition();
            }
            public void onNothingSelected(AdapterView<?> arg0) { }
        });
        spinCMO.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                ((TextView) arg0.getChildAt(0)).setTextColor(Color.BLACK);
                item2 = spinCMO.getSelectedItemPosition();
            }
            public void onNothingSelected(AdapterView<?> arg0) { }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ket  = editKet.getText().toString();
                String leasing = list_id.get(item);
                String cmo = list_id2.get(item2);

                if(ket.equals("")|| leasing.equals("") || cmo.equals("")){
                    Toast.makeText(GosurveyFormScreen.this, "Data tidak boleh kosong", Toast.LENGTH_SHORT).show();
                }
                else
                {
                        submit_form(leasing, cmo,ket);
                }
            }
        });
    }
    private void loadSpinnerData(String url) {

        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override

            public void onResponse(String response) {

                try{
                    List<String> list = new ArrayList<String>();
                    JSONArray jsonArray=new JSONArray(response);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        String name=jsonObject1.getString("name");
                        String id=jsonObject1.getString("id");
                        list_id.add(id);
                        list.add(name);
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinLeasing.setAdapter(dataAdapter);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }
    private void loadSpinnerData2(String url) {

        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override

            public void onResponse(String response) {

                try{
                    List<String> list = new ArrayList<String>();
                    JSONArray jsonArray=new JSONArray(response);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        String name=jsonObject1.getString("name");
                        String id=jsonObject1.getString("id");
                        list_id2.add(id);
                        list.add(name);
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinCMO.setAdapter(dataAdapter);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }
    private void submit_form(final String leasing_id, final String cmo_id, final String comment) {
        {
            //Showing the progress dialog
            final ProgressDialog loading = ProgressDialog.show(this, "Loading...", "Please wait...", false, false);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ServerPath,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            loading.dismiss();

                            //Showing toast message of the response
                            Toast.makeText(getApplicationContext(), "Berhasil mengajukan leasing!", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(GosurveyFormScreen.this, GoprosesDetail.class);
                            i.putExtra("id_gopros", id_gopros);
                            startActivity(i);
                            // Setting image as transparent after done uploading.
                            editKet.setText("");
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            loading.dismiss();

                            NetworkResponse networkResponse = volleyError.networkResponse;
                            if (networkResponse != null) {
                                Log.e("Volley", "Error. HTTP Status Code:"+networkResponse.statusCode);
                            }

                            if (volleyError instanceof TimeoutError) {
                                Log.e("Volley", "TimeoutError");
                            }else if(volleyError instanceof NoConnectionError){
                                Log.e("Volley", "NoConnectionError");
                            } else if (volleyError instanceof AuthFailureError) {
                                Log.e("Volley", "AuthFailureError");
                            } else if (volleyError instanceof ServerError) {
                                Log.e("Volley", "ServerError");
                            } else if (volleyError instanceof NetworkError) {
                                Log.e("Volley", "NetworkError");
                            } else if (volleyError instanceof ParseError) {
                                Log.e("Volley", "ParseError");
                            }
                            //Showing toast
                            Toast.makeText(GosurveyFormScreen.this, "error", Toast.LENGTH_LONG).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new Hashtable<String, String>();

                    //Adding parameters
                    params.put("leasing_id", leasing_id);
                    params.put("cmo_id", cmo_id);
                    params.put("comment", comment);
                    //returning parameters
                    return params;
                }
                @Override
                public Priority getPriority() {
                    return Priority.IMMEDIATE;
                }
            };

            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }

    }
}
