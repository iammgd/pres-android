package com.mitsubishidetagroup.apps.mitsubishi.Data;

public class Goproses {
    private int id,customer_phone;
    private String supervisor_name;
    private String sales_name;
    private String customer_name;
    private String customer_ktp;
    private String vehicle_name;
    private String created_at;
    private String source;
    private String progress;
    private String foto_ktp;
    private String status;
    private String status_validasi;
    public Goproses(){

    }

    Goproses(String status, int id, int customer_phone, String supervisor_name, String sales_name, String customer_name, String customer_ktp, String vehicle_name, String created_at, String source, String progress, String foto_ktp, String status_validasi){
        this.id=id;
        this.customer_phone=customer_phone;
        this.supervisor_name=supervisor_name;
        this.sales_name=sales_name;
        this.customer_name=customer_name;
        this.customer_ktp=customer_ktp;
        this.vehicle_name=vehicle_name;
        this.created_at=created_at;
        this.source=source;
        this.progress=progress;
        this.foto_ktp=foto_ktp;
        this.status = status;
        this.status_validasi = status_validasi;
    }

    public String getStatus_validasi() {
        return status_validasi;
    }

    public void setStatus_validasi(String status_validasi) {
        this.status_validasi = status_validasi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(int customer_phone) {
        this.customer_phone = customer_phone;
    }

    public String getSupervisor_name() {
        return supervisor_name;
    }

    public void setSupervisor_name(String supervisor_name) {
        this.supervisor_name = supervisor_name;
    }

    public String getSales_name() {
        return sales_name;
    }

    public void setSales_name(String sales_name) {
        this.sales_name = sales_name;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_ktp() {
        return customer_ktp;
    }

    public void setCustomer_ktp(String customer_ktp) {
        this.customer_ktp = customer_ktp;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getFoto_ktp() {
        return foto_ktp;
    }

    public void setFoto_ktp(String foto_ktp) {
        this.foto_ktp = foto_ktp;
    }
}
