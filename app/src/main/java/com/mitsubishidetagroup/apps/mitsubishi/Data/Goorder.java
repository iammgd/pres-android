package com.mitsubishidetagroup.apps.mitsubishi.Data;

/**
 * Created by megan on 27/05/2018.
 */

public class Goorder {
    private String nama_sales;
    private String nama_spv;
    private String nama_kacab;
    private String tanggal_pengajuan;
    private String vehicle_name;
    private String profit;

    private String status;
    private String nama_pelanggan,otr, bbn, diskon;
    private int id;
    private String status_validasi;

    public Goorder(){

    }

    Goorder(String nama_pelanggan, String status, int id, String nama_sales, String nama_spv, String nama_kacab, String tanggal_pengajuan, String vehicle_name, String otr, String bbn, String diskon, String status_validasi){
        this.nama_sales = nama_sales;
        this.nama_spv = nama_spv;
        this.nama_kacab = nama_kacab;
        this.tanggal_pengajuan = tanggal_pengajuan;
        this.vehicle_name = vehicle_name;
        this.otr = otr;
        this.bbn = bbn;
        this.diskon = diskon;
        this.id = id;
        this.nama_pelanggan = nama_pelanggan;
        this.status = status;
        this.profit = profit;
        this.status_validasi= status_validasi;
    }

    public void setStatus_validasi(String status_validasi) {
        this.status_validasi = status_validasi;
    }

    public String getStatus_validasi() {
        return status_validasi;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelanggan) {
        this.nama_pelanggan = nama_pelanggan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_sales() {
        return nama_sales;
    }

    public void setNama_sales(String nama_sales) {
        this.nama_sales = nama_sales;
    }

    public String getNama_spv() {
        return nama_spv;
    }

    public void setNama_spv(String nama_spv) {
        this.nama_spv = nama_spv;
    }

    public String getNama_kacab() {
        return nama_kacab;
    }

    public void setNama_kacab(String nama_kacab) {
        this.nama_kacab = nama_kacab;
    }

    public String getTanggal_pengajuan() {
        return tanggal_pengajuan;
    }

    public void setTanggal_pengajuan(String tanggal_pengajuan) {
        this.tanggal_pengajuan = tanggal_pengajuan;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getOtr() {
        return otr;
    }

    public void setOtr(String otr) {
        this.otr = otr;
    }

    public String getBbn() {
        return bbn;
    }

    public void setBbn(String bbn) {
        this.bbn = bbn;
    }

    public String getDiskon() {
        return diskon;
    }

    public void setDiskon(String diskon) {
        this.diskon = diskon;
    }
}
