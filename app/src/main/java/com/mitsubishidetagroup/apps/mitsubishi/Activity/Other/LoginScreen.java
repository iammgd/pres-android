package com.mitsubishidetagroup.apps.mitsubishi.Activity.Other;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SessionManager;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginScreen extends AppCompatActivity {
    private static final String TAG = LoginScreen.class.getSimpleName();
    private Button btnLogin;
    private EditText inputUsername;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    private String name,username_s,type,created_at,username_f,password_f,tag_string_req,errorMsg;
    private int uid,type_id;
    private JSONObject user,jObj,user_type, user_detail;
    private boolean  error;
    private  Map<String, String> params;
    private StringRequest strReq;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        inputUsername = (EditText) findViewById(R.id.txtusername);
        inputPassword = (EditText) findViewById(R.id.txtpassword);
        btnLogin = (Button) findViewById(R.id.btnlogin);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            intent = new Intent(LoginScreen.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                 username_f = inputUsername.getText().toString().trim();
                 password_f = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!username_f.isEmpty() && !password_f.isEmpty()) {
                    // login user
                    checkLogin(username_f, password_f);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Masukan Username dan Password", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });


    }
    private void checkLogin(final String username, final String password) {
        // Tag used to cancel the request
        tag_string_req = "req_login";
        pDialog.setMessage("Logging in ...");
        showDialog();
        strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();


                try {
                     jObj = new JSONObject(response);
                     error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
                        //data user
                        user = jObj.getJSONObject("user");
                        type_id = jObj.getInt("user_tipe");
                        uid = user.getInt("id");
                        username_s = user.getString("username");
                        type = user.getString("userable_id");
                        created_at = user.getString("created_at");
                        user_detail = user.getJSONObject("userable");
                        name = user_detail.getString("name");
                        // Inserting row in users table
                        db.addUser(name, username_s, uid, type, type_id, created_at);

                        // Launch main activity
                        intent = new Intent(LoginScreen.this,
                                MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in login. Get the error message
                        errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Pastikan anda terhubung ke internet!", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Pastikan anda terhubung ke internet!", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
