package com.mitsubishidetagroup.apps.mitsubishi.Data;

public class Gosurveys {
    private int id;
    private String leasing;
    private String tgl_masuk_leasing;
    private String nama_cmo;
    private String notelp_cmo;
    private String tgl_acc;
    private String status;
    private String status_validasi;


    public Gosurveys(){

    }

    Gosurveys(String status, int id, String leasing, String tgl_masuk_leasing, String nama_cmo, String notelp_cmo, String tgl_acc, String status_validasi){
        this.id=id;
        this.leasing=leasing;
        this.tgl_masuk_leasing = tgl_masuk_leasing;
        this.nama_cmo = nama_cmo;
        this.notelp_cmo = notelp_cmo;
        this.tgl_acc = tgl_acc;
        this.status = status;
        this.status_validasi=status_validasi;
    }

    public String getStatus_validasi() {
        return status_validasi;
    }

    public void setStatus_validasi(String status_validasi) {
        this.status_validasi = status_validasi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLeasing() {
        return leasing;
    }

    public void setLeasing(String leasing) {
        this.leasing = leasing;
    }

    public String getTgl_masuk_leasing() {
        return tgl_masuk_leasing;
    }

    public void setTgl_masuk_leasing(String tgl_masuk_leasing) {
        this.tgl_masuk_leasing = tgl_masuk_leasing;
    }

    public String getNama_cmo() {
        return nama_cmo;
    }

    public void setNama_cmo(String nama_cmo) {
        this.nama_cmo = nama_cmo;
    }

    public String getNotelp_cmo() {
        return notelp_cmo;
    }

    public void setNotelp_cmo(String notelp_cmo) {
        this.notelp_cmo = notelp_cmo;
    }

    public String getTgl_acc() {
        return tgl_acc;
    }

    public void setTgl_acc(String tgl_acc) {
        this.tgl_acc = tgl_acc;
    }


}
