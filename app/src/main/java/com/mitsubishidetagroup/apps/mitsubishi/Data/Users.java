package com.mitsubishidetagroup.apps.mitsubishi.Data;

public class Users {
    private int id,remember_token,userable_id,userable_type;
    private String username, password ,created_at,updated_at;

    Users(int id,int remember_token,int userable_id,int userable_type, String username, String password ,String created_at,String updated_at){
        this.id=id;
        this.remember_token=remember_token;
        this.userable_id=userable_id;
        this.userable_type=userable_type;
        this.username=username;
        this.password=password;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRemember_token(int remember_token) {
        this.remember_token = remember_token;
    }

    public void setUserable_id(int userable_id) {
        this.userable_id = userable_id;
    }

    public void setUserable_type(int userable_type) {
        this.userable_type = userable_type;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public int getRemember_token() {
        return remember_token;
    }

    public int getUserable_id() {
        return userable_id;
    }

    public int getUserable_type() {
        return userable_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getPassword() {
        return password;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getUsername() {
        return username;
    }
}
