package com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GosellRangkaForm;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GosurveyFormACC;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GosellView;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GosurveyView;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONException;
import org.json.JSONObject;

import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GAMBAR_KTP;

public class GosellTimDetail extends AppCompatActivity {
    public int user_type;
    TextView txtnama_customer, txtnotelp_customer, txtnoktp_customer, txtnama_komandan, txtnama_sales, txttanggal_gopros, txtsumber_prospek, txtprogress;
    TextView txtnama_kendaraan, txttipe_kendaraan, txtmodel_kendaraan, txtwarna_kendaraan, txttahun_produksi, txtketerangan_kendaraan;
    TextView txtleasing, txtnama_cmo, txtnotelp_cmo, txttgl_acc;
    TextView txtotr, txtdiskon, txtbbn, txtno_rangka, txtterakhir_diajukan;
    TextView txttim_nama, txttim_email, txttim_notelp, txtprofit;
    private SQLiteHandler db;

    public String id_gosell, no_rangka_string;
    private final String url_detail  = AppConfig.URL_DETAILGOSELL_TIM;
    ProgressDialog pDialog;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gosell_tim_detail);
        db = new SQLiteHandler(getApplicationContext());

        inisialisasi();
        callDetailGosurvey();
    }

    private void inisialisasi() {
        txtnama_customer = (TextView) findViewById(R.id.nama_customer);
        txtnotelp_customer = (TextView) findViewById(R.id.notelp_customer);
        txtnoktp_customer = (TextView) findViewById(R.id.noktp_customer);
        txtnama_komandan = (TextView) findViewById(R.id.nama_komandan);
        txtnama_sales = (TextView) findViewById(R.id.nama_sales);
        txttanggal_gopros = (TextView) findViewById(R.id.tanggal_gopros);
        txtsumber_prospek = (TextView) findViewById(R.id.sumber_prospek);
        txtprogress = (TextView) findViewById(R.id.progress);
        id_gosell = getIntent().getStringExtra("id_gosell");
        txtnama_kendaraan = (TextView) findViewById(R.id.vehicle_name);
        txttipe_kendaraan = (TextView) findViewById(R.id.vehicle_tipe);
        txtmodel_kendaraan = (TextView) findViewById(R.id.vehicle_model);
        txtwarna_kendaraan = (TextView) findViewById(R.id.vehicle_color);
        txttahun_produksi = (TextView) findViewById(R.id.vehicle_year);
        txtketerangan_kendaraan = (TextView) findViewById(R.id.vehicle_ket);
        txtleasing = (TextView) findViewById(R.id.leasing);
        txtnama_cmo = (TextView) findViewById(R.id.nama_cmo);
        txtnotelp_cmo = (TextView) findViewById(R.id.notelp_cmo);
        txttgl_acc = (TextView) findViewById(R.id.tgl_acc);
        txtprofit = (TextView) findViewById(R.id.txtprofit);

        txtotr = (TextView) findViewById(R.id.txtotr);
        txtdiskon = (TextView) findViewById(R.id.txtdiskon);
        txtbbn = (TextView) findViewById(R.id.txtbbn);
        txtno_rangka = (TextView) findViewById(R.id.txtno_rangka);
        txtterakhir_diajukan = (TextView) findViewById(R.id.txtterakhir_diajukan);
        txttim_nama = (TextView) findViewById(R.id.tim_nama);
        txttim_email = (TextView) findViewById(R.id.tim_email);
        txttim_notelp = (TextView) findViewById(R.id.tim_notelp);
        db = new SQLiteHandler(getApplicationContext());
        user_type = Integer.parseInt(db.getUserDetails().get("type_id"));

        button = findViewById(R.id.button);
        button.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View v )
            {
                if(user_type==6 || user_type == 7){
                    Toast.makeText(GosellTimDetail.this,
                            "Anda tidak memiliki akses untuk menambahkan/mengubah no rangka pada GoSell ini.", Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(GosellTimDetail.this, GosellRangkaForm.class);
                    i.putExtra("id_gosell", id_gosell);
                    startActivity(i);
                }

            }
        });
        if(user_type != 5){
            button.setVisibility(View.GONE);
        }
    }

    public void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(GosellTimDetail.this);
            pDialog.setMessage("Get data, please wait...");
            pDialog.setCancelable(false);
        }

        pDialog.show();
    }

    public void hideProgressDialog() {

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    private void callDetailGosurvey(){
        showProgressDialog();
        StringRequest strReq = new StringRequest(Request.Method.GET, url_detail + id_gosell, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("CEK RESPONSE: ", "Response " + response.toString());
                hideProgressDialog();
                try {
                    JSONObject object  = new JSONObject(response);
                    SetTextFromOBJtoView(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON ERROR: ", "Detail Goproses Error: " + error.getMessage());
                Toast.makeText(GosellTimDetail.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideProgressDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "JSON OBJECT");
    }

    private void SetTextFromOBJtoView(JSONObject object) throws JSONException {
        txtnama_customer.setText(object.getString("customer_name"));
        txtnotelp_customer.setText(object.getString("customer_phone"));
        txtnoktp_customer.setText(object.getString("customer_ktp"));
        txtnama_komandan.setText(object.getString("supervisor_name"));
        txtnama_sales.setText(object.getString("sales_name"));
        txttanggal_gopros.setText(object.getString("created_at"));
        txtsumber_prospek.setText(object.getString("source"));
        txtprogress.setText(object.getString("progress"));

        txtnama_kendaraan.setText(object.getString("vehicle_name"));
        txttipe_kendaraan.setText(object.getString("vehicle_tipe"));
        txtmodel_kendaraan.setText(object.getString("vehicle_model"));
        txtwarna_kendaraan.setText(object.getString("vehicle_warna"));
        txttahun_produksi.setText(object.getString("vehicle_thproduksi"));
        txtketerangan_kendaraan.setText(object.getString("vehicle_keterangan"));
        txtleasing.setText(object.getString("leasing"));
        txtnama_cmo.setText(object.getString("nama_cmo"));
        txtnotelp_cmo.setText(object.getString("notelp_cmo"));
        txttgl_acc.setText(object.getString("tgl_acc"));

        txtotr.setText(object.getString("otr"));
        txtdiskon.setText(object.getString("bbn"));
        txtbbn.setText(object.getString("diskon"));
        no_rangka_string = object.getString("no_rangka");
        txtno_rangka.setText(no_rangka_string);
        txtterakhir_diajukan.setText(object.getString("terakhir_diajukan"));
        txttim_nama.setText(object.getString("tim_gosell"));
        txttim_email.setText(object.getString("tim_email"));
        txttim_notelp.setText(object.getString("tim_phone"));
        txtprofit.setText(object.getString("profit"));

    }

    @Override
    public void onBackPressed() {
        finish();
        Intent i = new Intent(GosellTimDetail.this, GosellView.class);
        startActivity(i);
    }

}
