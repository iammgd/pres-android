package com.mitsubishidetagroup.apps.mitsubishi.Activity.Other;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoprosesView;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

public class AksesScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akses_screen);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(AksesScreen.this, MainActivity.class);
        startActivity(i);
    }
}
