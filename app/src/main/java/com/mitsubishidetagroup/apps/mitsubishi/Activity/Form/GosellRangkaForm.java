package com.mitsubishidetagroup.apps.mitsubishi.Activity.Form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosellTimDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosurveyDetail;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import java.util.Hashtable;
import java.util.Map;

import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_FILLGOSELL;
import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GOSURVEYACC;

public class GosellRangkaForm extends AppCompatActivity {
    private Button btnRangka;
    private TextView editRangka;
    private String URL_FORM = URL_FILLGOSELL;
    private String id_gosell;
    private ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gosell_rangka_form);

        inisialisasi();
        btnRangka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtRangka = editRangka.getText().toString();
                sendForm(txtRangka);
            }
        });
    }

    private void inisialisasi(){
        btnRangka = (Button) findViewById(R.id.btnRangka);
        editRangka = (TextView) findViewById(R.id.editRangka);

        id_gosell = getIntent().getStringExtra("id_gosell");
    }

    private void sendForm(final String txtRangka) {
        {
            //Showing the progress dialog
            final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_FORM + id_gosell,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            loading.dismiss();
                            Log.d("test", "onResponse: "+s);


                            //Showing toast message of the response
                            //Toast.makeText(GosurveyFormACC.this, s, Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(), "Berhasil menambahkan nomor rangka!", Toast.LENGTH_LONG).show();

                            // Setting after done
                            Intent i = new Intent(GosellRangkaForm.this, GosellTimDetail.class);
                            i.putExtra("id_gosell", id_gosell);
                            startActivity(i);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            loading.dismiss();
                            //Showing toast
                            Toast.makeText(GosellRangkaForm.this, "error", Toast.LENGTH_LONG).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String

                    //Creating parameters
                    Map<String, String> params = new Hashtable<String, String>();
                    //Adding parameters
                    params.put("no_rangka", txtRangka);
                    params.put("gosell_team_id", "1");
                    //returning parameters
                    return params;
                }
                @Override
                public Priority getPriority() {
                    return Priority.IMMEDIATE;
                }
            };

            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }

    }

}
