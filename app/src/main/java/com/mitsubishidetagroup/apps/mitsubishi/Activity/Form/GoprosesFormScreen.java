package com.mitsubishidetagroup.apps.mitsubishi.Activity.Form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosurveyDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoprosesView;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.JSONParser;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class GoprosesFormScreen extends AppCompatActivity {

    private Button SelectImageGallery;

    private   Bitmap lastBitmap = null;

    private int PICK_IMAGE_REQUEST = 1;

    private ProgressDialog progressDialog ;
    private SQLiteHandler db;
    private ImageView imageView;
    private String ServerUploadPath =AppConfig.URL_CREATEGOPROSES;
    private List<String> list_id = new ArrayList<String>();
    private AwesomeValidation awesomeValidation;
    private Button btnbuat;
    private int item;
    private EditText txtnama, txtprogress, txtsumber,txthp,txtktp;
    private Spinner spinKat, spinStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goproses_form_screen);
        db = new SQLiteHandler(getApplicationContext());
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        btnbuat = (Button) findViewById(R.id.btnSimpan);
        txtnama = (EditText) findViewById(R.id.editNama);
        txthp  = (EditText) findViewById(R.id.editHP);
        txtktp  = (EditText) findViewById(R.id.editKtp);
        txtprogress = (EditText) findViewById(R.id.editProgress);
        txtsumber = (EditText) findViewById(R.id.editSumber);
        spinKat = (Spinner) findViewById(R.id.spinKendaraan);
        spinStatus  = (Spinner) findViewById(R.id.spinBy);
        imageView = (ImageView)findViewById(R.id.imageView);
        awesomeValidation.addValidation(this, R.id.editHP, "[0-9]+", R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.editKtp, "[0-9]+", R.string.nameerror);

        SelectImageGallery = (Button)findViewById(R.id.buttonSelect);


        SelectImageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickImageIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickImageIntent.setType("image/*");
                pickImageIntent.putExtra("aspectX", 1);
                pickImageIntent.putExtra("aspectY", 1);
                pickImageIntent.putExtra("scale", true);
                pickImageIntent.putExtra("outputFormat",
                        Bitmap.CompressFormat.JPEG.toString());
                startActivityForResult(pickImageIntent, PICK_IMAGE_REQUEST);
            }
        });
        addItemOnSpinner();
        loadSpinnerData(AppConfig.URL_VEHICLE);
        spinKat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                ((TextView) arg0.getChildAt(0)).setTextColor(Color.BLACK);
                item = spinKat.getSelectedItemPosition();
            }
            public void onNothingSelected(AdapterView<?> arg0) { }
        });

        btnbuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama  = txtnama.getText().toString();
                String hp = txthp.getText().toString();
                String ktp = txtktp.getText().toString();
                String sumber = txtsumber.getText().toString();
                String progress = txtprogress.getText().toString();
                String kategori = list_id.get(item);
                String status = String.valueOf(spinStatus.getSelectedItemPosition());

                if(nama.equals("")|| hp.equals("") || ktp.equals("") || sumber.equals("") || kategori.equals("") || progress.equals("")){
                    Toast.makeText(GoprosesFormScreen.this, "Data tidak boleh kosong", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if (awesomeValidation.validate()) {
                        uploadImage(nama, hp, ktp, sumber, progress, kategori, status, db.getUserDetails().get("type"));
                    }
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

                lastBitmap = bitmap;
                //encoding image to string

                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(lastBitmap);
                //passing the image to volley
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void loadSpinnerData(String url) {

        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override

            public void onResponse(String response) {

                try{
                    List<String> list = new ArrayList<String>();
                    JSONArray jsonArray=new JSONArray(response);
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            String name=jsonObject1.getString("name");
                            String id=jsonObject1.getString("id");
                            list_id.add(id);
                            list.add(name);
                        }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinKat.setAdapter(dataAdapter);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }
    public void addItemOnSpinner() {
        List<String> list2 = new ArrayList<String>();
        list2.add("Non Cash");
        list2.add("Cash");
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinStatus.setAdapter(dataAdapter2);

    }
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return "data:image/jpeg;base64," + encodedImage;

    }

    private void uploadImage(final String nama, final String hp, final String ktp,
                             final String sumber,final String progress,final String kategori,final String status,final String id) {
        {
            //Showing the progress dialog
            final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ServerUploadPath,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            loading.dismiss();
                            Log.d("test", "onResponse: "+s);


                            //Showing toast message of the response
                            Toast.makeText(getApplicationContext(), "Berhasil membuat GoPros baru!", Toast.LENGTH_LONG).show();

                            // Setting image as transparent after done uploading.
                            imageView.setImageResource(android.R.color.transparent);
                            txtnama.setText("");
                            txthp.setText("");
                            txtktp.setText("");
                            txtprogress.setText("");
                            txtsumber.setText("");

                            // Setting after done
                            Intent i = new Intent(GoprosesFormScreen.this, GoprosesView.class);
                            startActivity(i);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            loading.dismiss();

                            NetworkResponse networkResponse = volleyError.networkResponse;
                            if (networkResponse != null) {
                                Log.e("Volley", "Error. HTTP Status Code:"+networkResponse.statusCode);
                            }

                            if (volleyError instanceof TimeoutError) {
                                Log.e("Volley", "TimeoutError");
                            }else if(volleyError instanceof NoConnectionError){
                                Log.e("Volley", "NoConnectionError");
                            } else if (volleyError instanceof AuthFailureError) {
                                Log.e("Volley", "AuthFailureError");
                            } else if (volleyError instanceof ServerError) {
                                Log.e("Volley", "ServerError");
                            } else if (volleyError instanceof NetworkError) {
                                Log.e("Volley", "NetworkError");
                            } else if (volleyError instanceof ParseError) {
                                Log.e("Volley", "ParseError");
                            }
                            //Showing toast
                            Toast.makeText(GoprosesFormScreen.this, "error", Toast.LENGTH_LONG).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    String image = getStringImage(lastBitmap);

                    //Creating parameters
                    Map<String, String> params = new Hashtable<String, String>();

                    //Adding parameters
                    params.put("photo_ktp", image);
                    params.put("name", nama);
                    params.put("phone_number", hp);
                    params.put("no_ktp", ktp);
                    params.put("source", sumber);
                    params.put("progress", progress);
                    params.put("vehicle_id", kategori);
                    params.put("sales_id", id);
                    params.put("cash", status);
                    //returning parameters
                    return params;
                }
                @Override
                public Priority getPriority() {
                    return Priority.IMMEDIATE;
                }
            };

            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }

    }
}
