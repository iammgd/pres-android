package com.mitsubishidetagroup.apps.mitsubishi.Activity.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GoorderJendralDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosurveyDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GoorderJendralAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GoprosesAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Adapter.GosurveyAdapter;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Goorder;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Goproses;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Gosurveys;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class GoorderJendralView extends AppCompatActivity {
    List<Goorder> goorderData = new ArrayList<Goorder>();
    ListView list;
    private static String URL_GOORDER;
    GoorderJendralAdapter adapter;
    private StringRequest strReq;
    private SQLiteHandler db;
    public String user_type, user_id;

    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goorder_jendral_view);

        inisialisasi();
        callGoOrder();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String id_goorder = ((TextView)view.findViewById(R.id.hiddenID_goorder_jendral)).getText().toString();
                Intent i = new Intent(GoorderJendralView.this, GoorderJendralDetail.class);
                i.putExtra("id_goorder", id_goorder);
                startActivity(i);
            }
        });
    }

    public void inisialisasi()
    {
        db = new SQLiteHandler(getApplicationContext());
        user_type = db.getUserDetails().get("type_id").toString();
        user_id = db.getUserDetails().get("uid").toString();
        URL_GOORDER = AppConfig.URL_GETGOORDER_JENDRAL + "?user_id="+user_id+"&user_type=" + user_type;
        list = (ListView) findViewById(R.id.list_goorder_jendral);

        adapter = new GoorderJendralAdapter(GoorderJendralView.this, goorderData);
        //adapter.setCustomButtonListner(getActivity(),this);
        list.setAdapter(adapter);
    }

    public void callGoOrder() {
        showProgressDialog();

        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(URL_GOORDER, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("CEK RESPONSES: ", response.toString());
                hideProgressDialog();
                if (response.length() > 0) {
                    // Parsing json
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            Goorder goorder = createGoprosesFromJSONObject(obj);
                            goorderData.add(goorder);

                        } catch (JSONException e) {
                            Log.e("CEK ERROR REPONSES: ", "JSON Parsing error: " + e.getMessage());
                            hideProgressDialog();
                        }
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("CEK ERROR: "+error.getMessage(), "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
    }

    public void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(GoorderJendralView.this);
            pDialog.setMessage("Get data, please wait...");
            pDialog.setCancelable(false);
        }

        pDialog.show();
    }

    public void hideProgressDialog() {

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    private Goorder createGoprosesFromJSONObject(JSONObject object) throws JSONException {

        Goorder goorder = new Goorder();
        goorder.setId(object.getInt("id"));
        goorder.setNama_pelanggan(object.getString("customer_name"));
        goorder.setNama_sales(object.getString("sales_name"));
        goorder.setNama_spv(object.getString("spv_name")+":");
        goorder.setNama_kacab(object.getString("kacab_name"));
        goorder.setNama_kacab(object.getString("kacab_name"));
        goorder.setTanggal_pengajuan(object.getString("tanggal_pengajuan"));
        goorder.setVehicle_name(object.getString("vehicle_name"));
        goorder.setOtr(object.getString("otr"));
        goorder.setBbn(object.getString("bbn"));
        goorder.setDiskon(object.getString("diskon"));
        goorder.setStatus(object.getString("status"));
        goorder.setStatus_validasi(object.getString("status_validasi"));

        return goorder;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(GoorderJendralView.this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.legenda,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent i;
        switch (item.getItemId()){
            case R.id.legenda:
                Toast.makeText(getApplicationContext(), "Legenda terbuka.", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }
}
