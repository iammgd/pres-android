package com.mitsubishidetagroup.apps.mitsubishi.Data;

public class Vehicles {
    private  int id;
    private  String name, type, model, color, production_year, comment, created_at, updated_at;
    Vehicles(int id,String name,String type,String model,String color,String production_year,String comment,String created_at,String updated_at){
        this.id=id;
        this.name=name;
        this.type=type;
        this.model=model;
        this.color=color;
        this.production_year=production_year;
        this.comment=comment;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setProduction_year(String production_year) {
        this.production_year = production_year;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public String getComment() {
        return comment;
    }

    public String getModel() {
        return model;
    }

    public String getProduction_year() {
        return production_year;
    }

    public String getType() {
        return type;
    }
}
