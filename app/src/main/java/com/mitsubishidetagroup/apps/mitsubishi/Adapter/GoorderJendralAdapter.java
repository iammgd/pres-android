package com.mitsubishidetagroup.apps.mitsubishi.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mitsubishidetagroup.apps.mitsubishi.Data.Goorder;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Goproses;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import java.util.List;

/**
 * Created by megan on 27/05/2018.
 */

public class GoorderJendralAdapter extends BaseAdapter {
    private Activity activity;
    private List<Goorder> goorderData;
    private static LayoutInflater inflater=null;

    public GoorderJendralAdapter(Activity a, List<Goorder> goorderData) {
        activity = a;
        this.goorderData=goorderData;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return goorderData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)

            vi = inflater.inflate(R.layout.activity_goorder_jendral_list, null);

        TextView tanggalPengajuan = (TextView)vi.findViewById(R.id.tanggal_pengajuan);
        TextView status = (TextView)vi.findViewById(R.id.spv_name);
        TextView customerName = (TextView)vi.findViewById(R.id.customer_name);
        TextView hiddenid = (TextView)vi.findViewById(R.id.hiddenID_goorder_jendral);
        ImageView imageView= (ImageView)vi.findViewById(R.id.imageView);

        Goorder goorder = goorderData.get(position);

        tanggalPengajuan.setText(goorder.getTanggal_pengajuan());
        status.setText("Status: " +goorder.getStatus());
        customerName.setText(goorder.getNama_pelanggan());
        hiddenid.setText( String.valueOf(goorder.getId()));
        if(goorder.getStatus_validasi().equals("1")){
            imageView.setImageResource(R.drawable.goorderaccepted);
        }else if(goorder.getStatus_validasi().equals("2")){
            imageView.setImageResource(R.drawable.goordersubmitted);
        }else if(goorder.getStatus_validasi().equals("3")){
            imageView.setImageResource(R.drawable.goordersubmitted);
        }else if(goorder.getStatus_validasi().equals("4")){
            imageView.setImageResource(R.drawable.goorderdefault);
        }else {
            imageView.setImageResource(R.drawable.goorderred);
        }
        return vi;
    }

}
