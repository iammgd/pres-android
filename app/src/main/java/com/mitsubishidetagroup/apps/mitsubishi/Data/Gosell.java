package com.mitsubishidetagroup.apps.mitsubishi.Data;

/**
 * Created by megan on 28/05/2018.
 */

public class Gosell {
    int id;
    String customer_name, vehicle_name, profit;
    private String status_validasi;


    public Gosell(){

    }

    Gosell(int id, String customer_name, String vehicle_name, String profit, String status_validasi){
        this.id = id;
        this.customer_name = customer_name;
        this.vehicle_name = vehicle_name;
        this.profit = profit;
        this.status_validasi=status_validasi;

    }

    public void setStatus_validasi(String status_validasi) {
        this.status_validasi = status_validasi;
    }

    public String getStatus_validasi() {
        return status_validasi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }
}
