package com.mitsubishidetagroup.apps.mitsubishi.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mitsubishidetagroup.apps.mitsubishi.Data.Goproses;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Gosurveys;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import java.util.List;

/**
 * Created by megan on 25/05/2018.
 */

public class GosurveyAdapter extends BaseAdapter{
    private Activity activity;
    private List<Gosurveys> gosurveyData;
    private static LayoutInflater inflater=null;

    public GosurveyAdapter(Activity a, List<Gosurveys> gosurveyData) {
        activity = a;
        this.gosurveyData=gosurveyData;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return gosurveyData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)

            vi = inflater.inflate(R.layout.activity_gosurvey_list, null);

        TextView txtLeasing = (TextView)vi.findViewById(R.id.txtLeasing);
        TextView txtTglMasukLeasing = (TextView)vi.findViewById(R.id.txtTglMasukLeasing);
        TextView txtNamaCMO = (TextView)vi.findViewById(R.id.txtNamaCMO);
        TextView txtTanggalACC = (TextView)vi.findViewById(R.id.txtTanggalACC);
        TextView hiddenid = (TextView)vi.findViewById(R.id.hiddenID_gosurvey);
        ImageView imageView= (ImageView)vi.findViewById(R.id.imageView);

        Gosurveys gosurvey = gosurveyData.get(position);

        txtLeasing.setText(gosurvey.getLeasing());
        txtTglMasukLeasing.setText(gosurvey.getTgl_masuk_leasing());
        txtNamaCMO.setText("CMO: "+gosurvey.getNama_cmo());
        txtTanggalACC.setText(gosurvey.getTgl_acc());
        hiddenid.setText( String.valueOf(gosurvey.getId()));
        if(gosurvey.getStatus_validasi().equals("0")){
            imageView.setImageResource(R.drawable.gosurveydefault);
        }else if(gosurvey.getStatus_validasi().equals("1")){
            imageView.setImageResource(R.drawable.gosurveyaccepted);
        } else {
            imageView.setImageResource(R.drawable.gosurveyrejected); //ditolak
        }
        return vi;
    }

}
