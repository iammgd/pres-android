package com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GosurveyFormACC;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GosurveyView;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig;
import com.mitsubishidetagroup.apps.mitsubishi.Config.AppController;
import com.mitsubishidetagroup.apps.mitsubishi.Config.SQLiteHandler;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONException;
import org.json.JSONObject;

import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GAMBAR_KTP;

public class GosurveyDetail extends AppCompatActivity {
    private SQLiteHandler db;
    private int user_type;
    NetworkImageView foto_ktp;
    TextView txtnama_customer, txtnotelp_customer, txtnoktp_customer, txtnama_komandan, txtnama_sales, txttanggal_gopros, txtsumber_prospek, txtprogress;
    TextView txtnama_kendaraan, txttipe_kendaraan, txtmodel_kendaraan, txtwarna_kendaraan, txttahun_produksi, txtketerangan_kendaraan;
    TextView txtleasing, txtnama_cmo, txtnotelp_cmo, txttgl_acc, txtstatus;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public String id_gosurvey, tgl_acc;
    public int status_validasi;
    private final String url_detail  = AppConfig.URL_GOSURVEYDETAIL;
    ProgressDialog pDialog;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gosurvey_detail);

        inisialisasi();
        callDetailGosurvey();

        button.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View v )
            {
                if(user_type == 2 || user_type == 3){
                    if(status_validasi == 0){
                        Intent i = new Intent(GosurveyDetail.this, GosurveyFormACC.class);
                        i.putExtra("id_gosurvey", id_gosurvey);
                        startActivity(i);
                    } else if(status_validasi == 2){
                        Toast.makeText(getApplicationContext(), "Leasing pada GoSurvey ini sudah ditolak!", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Leasing pada GoSurvey ini sudah diterima!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Anda tidak memiliki akses untuk menerima leasing pada GoSurvey ini!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void inisialisasi() {
        foto_ktp = (NetworkImageView) findViewById(R.id.foto_ktp);
        txtnama_customer = (TextView) findViewById(R.id.nama_customer);
        txtnotelp_customer = (TextView) findViewById(R.id.notelp_customer);
        txtnoktp_customer = (TextView) findViewById(R.id.noktp_customer);
        txtnama_komandan = (TextView) findViewById(R.id.nama_komandan);
        txtnama_sales = (TextView) findViewById(R.id.nama_sales);
        txttanggal_gopros = (TextView) findViewById(R.id.tanggal_gopros);
        txtsumber_prospek = (TextView) findViewById(R.id.sumber_prospek);
        txtprogress = (TextView) findViewById(R.id.progress);
        id_gosurvey = getIntent().getStringExtra("id_gosurvey");
        txtnama_kendaraan = (TextView) findViewById(R.id.vehicle_name);
        txttipe_kendaraan = (TextView) findViewById(R.id.vehicle_tipe);
        txtmodel_kendaraan = (TextView) findViewById(R.id.vehicle_model);
        txtwarna_kendaraan = (TextView) findViewById(R.id.vehicle_color);
        txttahun_produksi = (TextView) findViewById(R.id.vehicle_year);
        txtketerangan_kendaraan = (TextView) findViewById(R.id.vehicle_ket);
        txtleasing = (TextView) findViewById(R.id.leasing);
        txtnama_cmo = (TextView) findViewById(R.id.nama_cmo);
        txtnotelp_cmo = (TextView) findViewById(R.id.notelp_cmo);
        txttgl_acc = (TextView) findViewById(R.id.tgl_acc);
        txtstatus = (TextView) findViewById(R.id.gosurvey_status);
        button = findViewById(R.id.button);
        db = new SQLiteHandler(getApplicationContext());
        user_type= Integer.parseInt(db.getUserDetails().get("type_id"));

    }

    public void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(GosurveyDetail.this);
            pDialog.setMessage("Get data, please wait...");
            pDialog.setCancelable(false);
        }

        pDialog.show();
    }

    public void hideProgressDialog() {

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    private void callDetailGosurvey(){
        showProgressDialog();
        StringRequest strReq = new StringRequest(Request.Method.GET, url_detail + id_gosurvey, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("CEK RESPONSE: ", "Response " + response.toString());
                hideProgressDialog();
                try {
                    JSONObject object  = new JSONObject(response);
                    SetTextFromOBJtoView(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON ERROR: ", "Detail Goproses Error: " + error.getMessage());
                Toast.makeText(GosurveyDetail.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideProgressDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "JSON OBJECT");
    }

    private void SetTextFromOBJtoView(JSONObject object) throws JSONException {
        String gambar_ktp  = URL_GAMBAR_KTP+ object.getString("foto_ktp");

        txtnama_customer.setText(object.getString("customer_name"));
        txtnotelp_customer.setText(object.getString("customer_phone"));
        txtnoktp_customer.setText(object.getString("customer_ktp"));
        txtnama_komandan.setText(object.getString("supervisor_name"));
        txtnama_sales.setText(object.getString("sales_name"));
        txttanggal_gopros.setText(object.getString("created_at"));
        txtsumber_prospek.setText(object.getString("source"));
        txtprogress.setText(object.getString("progress"));
        foto_ktp.setImageUrl(gambar_ktp, imageLoader);

        txtnama_kendaraan.setText(object.getString("vehicle_name"));
        txttipe_kendaraan.setText(object.getString("vehicle_tipe"));
        txtmodel_kendaraan.setText(object.getString("vehicle_model"));
        txtwarna_kendaraan.setText(object.getString("vehicle_warna"));
        txttahun_produksi.setText(object.getString("vehicle_thproduksi"));
        txtketerangan_kendaraan.setText(object.getString("vehicle_keterangan"));
        txtleasing.setText(object.getString("leasing"));
        txtnama_cmo.setText(object.getString("nama_cmo"));
        txtnotelp_cmo.setText(object.getString("notelp_cmo"));
        txtstatus.setText(object.getString("status"));
        tgl_acc = object.getString("tgl_acc");
        txttgl_acc.setText(tgl_acc);
        status_validasi = object.getInt("status_validasi");

    }

    @Override
    public void onBackPressed() {
        finish();
        Intent i = new Intent(GosurveyDetail.this, GosurveyView.class);
        startActivity(i);
    }

}
