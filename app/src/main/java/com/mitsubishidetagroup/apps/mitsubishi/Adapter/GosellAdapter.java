package com.mitsubishidetagroup.apps.mitsubishi.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mitsubishidetagroup.apps.mitsubishi.Data.Goproses;
import com.mitsubishidetagroup.apps.mitsubishi.Data.Gosell;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import java.util.List;

/**
 * Created by megan on 28/05/2018.
 */

public class GosellAdapter extends BaseAdapter{
    private Activity activity;
    private List<Gosell> gosellData;
    private static LayoutInflater inflater=null;

    public GosellAdapter(Activity a, List<Gosell> gosellData) {
        activity = a;
        this.gosellData=gosellData;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return gosellData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)

            vi = inflater.inflate(R.layout.activity_gosell_list, null);

        TextView custName = (TextView)vi.findViewById(R.id.txtNamaPelanggan);
        TextView vehicleName = (TextView)vi.findViewById(R.id.txtVehicle);
        TextView profit = (TextView)vi.findViewById(R.id.txtProfit);
        TextView hiddenid = (TextView)vi.findViewById(R.id.hiddenID_gosell);
        ImageView imageView= (ImageView)vi.findViewById(R.id.imageView);

        Gosell gosell = gosellData.get(position);

        custName.setText(gosell.getCustomer_name());
        profit.setText(gosell.getProfit());
        vehicleName.setText(gosell.getVehicle_name());
        hiddenid.setText( String.valueOf(gosell.getId()));
        if(gosell.getStatus_validasi().equals("0")){
            imageView.setImageResource(R.drawable.goselldefault);
        }else{
            imageView.setImageResource(R.drawable.gosellaccepted);
        }
        return vi;
    }

}
