package com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Form.GoorderJendralPengajuanForm;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import java.util.Hashtable;
import java.util.Map;

import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GOORDERREJECT_PANGLIMA;
import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GOORDERREQUEST_JENDRAL;

public class GoorderPanglimaDetailTolak extends AppCompatActivity {
    private Button btnSimpan;
    private TextView txtAlasanTolak;
    private String URL_FORM = URL_GOORDERREJECT_PANGLIMA;
    private String id_goorder;
    private ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goorder_panglima_detail_tolak);

        inisialisasi();

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String alasan  = txtAlasanTolak.getText().toString();
                sendForm(alasan);
            }
        });
    }

    private void inisialisasi(){
        btnSimpan = (Button) findViewById(R.id.btnSimpan);
        txtAlasanTolak = (TextView) findViewById(R.id.editAlasanTolak);
        id_goorder = getIntent().getStringExtra("id_goorder");
    }

    private void sendForm(final String alasan) {
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Sending...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_FORM + id_goorder,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();
                        Log.d("test", "onResponse: "+s);


                        //Showing toast message of the response
                        //Toast.makeText(GosurveyFormACC.this, s, Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "GoOrder telah ditolak!", Toast.LENGTH_LONG).show();

                        // Setting after done
                        Intent i = new Intent(GoorderPanglimaDetailTolak.this, GoorderPanglimaDetail.class);
                        i.putExtra("id_goorder", id_goorder);
                        startActivity(i);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        //Showing toast
                        Toast.makeText(GoorderPanglimaDetailTolak.this, "error", Toast.LENGTH_LONG).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("reason", alasan);
                //returning parameters
                return params;
            }
            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
