package com.mitsubishidetagroup.apps.mitsubishi.Activity.Form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Detail.GosurveyDetail;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.Other.LoginScreen;
import com.mitsubishidetagroup.apps.mitsubishi.Activity.View.GoprosesView;
import com.mitsubishidetagroup.apps.mitsubishi.MainActivity;
import com.mitsubishidetagroup.apps.mitsubishi.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import static com.mitsubishidetagroup.apps.mitsubishi.Config.AppConfig.URL_GOSURVEYACC;

public class GosurveyFormACC extends AppCompatActivity {
    private Button btnAcc;
    private Spinner spinAcc;
    private String URL_FORM = URL_GOSURVEYACC;
    private String id_gosurvey, message;
    private ProgressDialog progressDialog;
    private JSONObject jObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gosurvey_form_acc);

        inisialisasi();

        btnAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtACC = String.valueOf(spinAcc.getSelectedItem());
                String txtsetACC;
                if(txtACC.equals("Terima")){
                    txtsetACC = "1";
                } else {
                    txtsetACC = "0";
                }
                sendForm(txtsetACC);
            }
        });
    }

    private void inisialisasi(){
        btnAcc = (Button) findViewById(R.id.btnAcc);
        spinAcc = (Spinner) findViewById(R.id.spinAcc);

        id_gosurvey = getIntent().getStringExtra("id_gosurvey");
        addItemOnSpinnerAcc();
    }

    private void sendForm(final String txtsetACC) {
        {
            //Showing the progress dialog
            final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_FORM + id_gosurvey,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            try {
                                jObj = new JSONObject(s);

                                message = jObj.getString("message");
                                //Disimissing the progress dialog
                                loading.dismiss();
                                Log.d("test", "onResponse: "+s);


                                //Showing toast message of the response
                                //Toast.makeText(GosurveyFormACC.this, s, Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                                // Setting after done
                                Intent i = new Intent(GosurveyFormACC.this, GosurveyDetail.class);
                                i.putExtra("id_gosurvey", id_gosurvey);
                                startActivity(i);

                            } catch (JSONException e) {
                                // JSON error
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Pastikan anda terhubung ke internet!", Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            loading.dismiss();
                            //Showing toast
                            Toast.makeText(GosurveyFormACC.this, "error", Toast.LENGTH_LONG).show();

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String

                    //Creating parameters
                    Map<String, String> params = new Hashtable<String, String>();
                    //Adding parameters
                    params.put("acc", txtsetACC);
                    //returning parameters
                    return params;
                }
                @Override
                public Priority getPriority() {
                    return Priority.IMMEDIATE;
                }
            };

            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }

    }

    public void addItemOnSpinnerAcc() {
        List<String> list2 = new ArrayList<String>();
        list2.add("Terima");
        list2.add("Tolak");
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinAcc.setAdapter(dataAdapter2);

    }
}
